= Fedora Workstation & Spins User Guide

Welcome to the Fedora Workstation & Spins User Guide!  

This is a user guide for Fedora Workstation that aims to also support users of the widespread KDE spin! Other spins might be added later if related maintainers volunteer.  

The guide is problem-oriented, "upstream"-focused, and provides guidance for beginners and advanced users. At pages (or problems) where advanced users might seek more detailed information, **"advanced" boxes** will be provided for advanced users at the relevant places. KDE users should follow the problem-oriented structure the same way as Workstation users, and if there are differences between Workstation and KDE in the given case, there will be a box in place informing you about the difference and if necessary, linking to an additional KDE-specific section.  

**It is intended to have the first pages published during the first half of the Fedora 37 release cycle!** Keep watching this page and **the menu on the left!**  

image::fedora-workstation-logo.png[Fedora Workstation] 
image::kde-banner.png[Fedora KDE Spin]  

== Introduction to Fedora Workstation & Spins

**Fedora Workstation** is a reliable, user-friendly, and powerful operating system for your laptop or desktop computer. It supports a wide range of developers, from hobbyists and students to professionals in corporate environments.  

Fedora Workstation uses Fedora's default desktop environment GNOME, but if you prefer an alternative desktop environment such as KDE Plasma Desktop, you can get it by using the **spin** that deploys your preferred desktop environment. Besides the different desktop environments, spins are pre-configured for desktop use cases the same way Fedora Workstation is (e.g., in terms of default firewall rules).  

You can download Fedora Workstation https://getfedora.org/workstation/[here] and your preferred spin https://spins.fedoraproject.org/[here].  

== About this guide

This guide will focus on Fedora Workstation. However, since the KDE spin is also in widespread use, this guide's instructions will make clear if they only apply to Workstation or if they also apply to KDE. When instructions only apply to Workstation, a box will make aware of the fact, and explain the differences or, if necessary, link to an additional KDE-specific section at the **bottom section** of the page, where sufficient elaboration of the differences in the KDE spin are available in order to enable you to achieve the same outcome on KDE. Other spins can be added if members volunteers to create and maintain them.  

Advanced users should check the **"advanced" boxes** (the "Choose and install Workstation or Spins" category, containing the pages of the "Fedora Anaconda Web-UI Guide", also links to "module" pages for advanced users, additionally to the "advanced" boxes). Generally, the pages of the Workstation & Spins User Guide will be designed for the https://en.wikipedia.org/wiki/KISS_principle[KISS] principle, and focused on providing only information relevant for the targeted audience and the targeted intention: as far as possible, the guide will exploit the fact that Fedora keeps compatibibility upstream and sets upstream Docs in contrast to Fedora and to each other: wherever possible, we will provide links upstream (and/or compare the upstream in relation to Fedora) to ensure that users always get the most up to date and extensive information. Also, this helps avoiding redundancy and thus, avoids the appearance of inconsistency over time. The increased generic character, which is indirectly facilitated by this focus, also makes it easier for users to find their way to their goal.  

Problem-oriented KISS-focused information does **not** need to be split in different pages for beginner and advanced users: the provided information aims to solve the problem and/or achieve the targeted goal the most comprehensible and efficient way. At this place, the upstream Docs will be selected to facilitate this. If necessary, upstream Docs will be complemented by examples to release beginners from interpreting complex Docs (such as systemd) and enable them to achieve their goal. Advanced users will find more details and more sophisticated upstream Docs in the "advanced" boxes.  

The problem-orientation of the structure aims to mitigate that users need to have any knowledge of the problem/issue/task in advance before they can find the desired page. This applies to both beginners and advanced users, although on different levels. The problem-orientation also facilitates the combination of Workstation with spins. Pages, content, menu items, and their paths and sequences should correspond to the reasoning/considerations of users with related problems.  

Additionally, the old Install Guide of the release Docs will be replaced by a new guide that will find its home here (the approach, structure and content of the new guide will not be comparable to the old one though). However, the new guide will focus from the beginning on the new cockpit-based Web-UI installation tool: it has **not yet been determined when the Web-UI will be officially released** to become the default installation tool of Fedora Workstation and its spins.   

Nevertheless, we aim to provide a publishable **Web-UI Guide** "release candidate" during the **first half of the Fedora 37 release lifecycle**. At the same period, it is planned to already provide an **unofficial** Fedora 37 with the new Web-UI. However, the official Workstation and its spins will still remain with the GTK installation tool until we decide the Web-UI to be ready for release. We aim to keep the Web-UI Guide on par with the developments of the Web-UI from the beginning, in order to facilitate and encourage users to test it and provide feedback. Although Docs issue tickets are intended for Docs pages and content, _it is exceptionally ok to provide feedback about the Web-UI installation over Docs issue tickets_ if that makes it easier for you (we will forward the information to the Anaconda team, which develops the Web-UI).  

The new Web-UI Guide will be not just an installation guide but a guide that contains everything up to the installed Fedora Linux. Its pages will be found in the category "Choose and install Workstation or Spins".  

== Background of the guide

This guide aims to prioritize quality over quantity: it will **only publish pages that have maintainers** who keep their pages up to date (checks per release cycle)! There will be **no obligation** to contribute persistently working time, but once a page is no longer maintained it shall be removed from published content/pages (or transferred to a maintainer who can at least check regularly if updates are necessary and remove it from the published content/pages once updates are necessary). If you are interested in contributing to this guide, feel free to read the https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/raw/main/MAINTAINER.md[MAINTAINER.md] for more information! It also contains the intended menu structure, which can be discussed in the issue tickets.  

If you only want to add/change content of an existing page, **feel free to contribute as usual in our Docs**! The maintainers will decide if they can keep the added/changed content maintained (maintenance of a page can also be shared among >1 maintainers). If you want to add and maintain pages (or take over maintenance of an existing page that is currently offline), open a ticket or create a merge request so that we can discuss how to place it in the structure (or let us know in the https://chat.fedoraproject.org/#/room/#docs:fedoraproject.org[Docs Matrix channel]).  

== Support & common issues

Our first line support is https://ask.fedoraproject.org/[Ask.Fedora].  

If you are unsure where to go or what to do, Ask.Fedora is the place you should start at.  

Commonly-encountered problems for current releases of Fedora Linux, and (if available) fixes or workarounds for these issues can be found at the https://ask.fedoraproject.org/c/common-issues/[Common Issues]. Before filing a bug or opening a topic at https://ask.fedoraproject.org/[Ask.Fedora], you should search your problem there. If you do not find your problem there, but it seems likely to affect a large number of Fedora users, suggest a new entry in the https://ask.fedoraproject.org/c/common-issues/common-issues-proposed/[Proposed Common Issues category].  

You can also visit us on https://chat.fedoraproject.org/#/room/#fedora:fedoraproject.org[Matrix].
